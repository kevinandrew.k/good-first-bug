# Good First Bug


This is a platform for anyone to get exposure to FLOSS contributions. The best way to start contributing to FLOSS is to by attending beginner's bugs. Even though there are lots of content  available on Internet , many fails  to accomplish fixing the first bug due various starting troubles. The 'Good First Bug' initiative to address such gap. 

FSMK wants to host Good First Bug mainly in colleges. However depends on the demand or need we could plan to host this in other common spaces as well.

## Duration of Program
Approximately 3-4 hours

## Human Resource

- Event Coordinator (Arranging Venue, - Coordinating with volunteers, Announcing Event etc)
- Technical Specialist (Preparing the content, Handle workshops, )
- Volunteers (Support participants during the event )
- PR Coordinator (Content creation, Campaign material creation)

## Program structure
| Duration | Activity | Remarks |
|--|--|--|
| 15 Min  | FSMK Induction  ||
| 15 Min | Team Building Activity ||
| 1 Hours   | Walk through of where/how to start contribute ?  * Demo on fixing a bug * Raising a PR * Reporting Issue * Present sources where   ||
| 2 Hours     | Bug fixing||

## Reference

- https://www.freecodecamp.org/news/finding-your-first-open-source-project-or-bug-to-work-on-1712f651e5ba/

- https://wiki.mozilla.org/Good_first_bug



## Upcoming Events

### [Good First Bug 1](https://gitlab.com/fsmk/fsmk-projects/good-first-bug/-/blob/main/1/README.md)

## Completed Events

